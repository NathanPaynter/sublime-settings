My Sublime Settings
=======
>Hosting here so I can copy to other computers cause I'm too lazy to use a usb stick...

### Installed Packages

* Color Highlighter
* JSON-Template
* LESS
* Markdown Preview
* Package Control (Obviously)
* Sass
* SideBarEnhancements
* SublimeLinter
  * contrib-lessc
  * csslint
  * json
* Themes
  * Afterglow
  * Seti_UI
  * Seti_UX
* DocBlockr
* Themr
